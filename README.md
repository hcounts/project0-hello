# Proj0-Hello
-------------
Author: Harrison Counts
Contact: hcounts4@uoregon.edu

Description:
When calling the 'make run' command in your bash terminal, this program will print "Hello, world".
